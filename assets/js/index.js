let startPage = 1;
let currentData = [];
let totalItems;
let currentPage = 1;

$(document).ready(function() {
  //setting auth creds in local storage
  // TO BE REMOVED!
  let obj = {
    "Content-Type": "application/json",
    "X-I2CE-ENTERPRISE-ID": "aiva_homes",
    "X-I2CE-USER-ID": "mtywnzmzmzq2mcawoxf5ahh6bxh5eg__",
    "X-I2CE-API-KEY":
      "bXR5d256bXptenEybWNhd294ZjVhaGg2YnhoNWVnMTYwNzMzMzQ2MCAzNg__"
  };
  localStorage.setItem("authentication", JSON.stringify(obj));

  if (getCookie("authentication")) {
    // check if any search keyword is already applied..
    $("#search-input").val(localStorage.getItem("search_keywords"));

    // put a loader till api finishes
    $("#container").append("<div class='loader'></div>");
    let filters = {};
    if (localStorage.getItem("searched") == "true") {
      filters["keywords"] = "~" + localStorage.getItem("search_keywords");
    }

    get_scenes(
      filters,
      res => {
        $("#container").empty();
        currentData = res.data;
        totalItems = res.total_number;
        updateContainerData(res.data);
        updatePagination();
      },
      err => console.log(err)
    );
  }
});

// gets index of page and calls api passing page_number equal to that index
function loadPageData(idx) {
  $("#container").empty();
  $("#pagination").empty();

  currentPage = idx;

  let filters = {};
  filters["page_number"] = idx;

  // check if any keyword is applied for serach
  if (localStorage.getItem("searched") == "true") {
    filters["keywords"] = "~" + localStorage.getItem("search_keywords");
  }
  $("#container").append("<div class='loader'></div>");
  get_scenes(
    filters,
    res => {
      $("#container").empty();
      currentData = res.data;
      totalItems = res.total_number;

      updateContainerData(res.data);
      updatePagination();
    },
    err => console.log(err)
  );
}

// Tiggers when input text is changed, calls api passing search keywords
$("input").change(function() {
  const val = $("#search-input").val();
  localStorage.setItem("search_keywords", val);
  $("#container").empty();
  $("#pagination").empty();

  currentPage = 1;

  $("#container").append("<div class='loader'></div>");
  get_scenes(
    {
      keywords: "~" + val
    },
    res => {
      $("#container").empty();
      currentData = res.data;
      totalItems = res.total_number;
      updateContainerData(res.data);
      updatePagination();
    },
    err => console.log(err)
  );
});

// Hanlder to add HTML content from fetched data
function updateContainerData(data) {
  if (getCookie("search_keywords")) {
    $("#container").append(
      "<div id='result-text' style='width:100vw'><h2>Found " +
        totalItems +
        " results for " +
        getCookie("search_keywords") +
        " </h2></div>"
    );
  }

  data.map((row, idx) =>
    $("#container").append(
      "<div class='card number-" +
        (idx % 3) +
        "'><div class='card-img-wrapper' ><a ><img onClick='imageClickHandler(" +
        idx +
        ")' src='" +
        row.thumbnail +
        "' /></a></div><div class='card-info-wrapper'><div class='image-info'><h3><b>Modern Home Workspace</b></h3> <h4><b>PIDILITE</b></h4></div><div class='options'><i class='fa fa-heart' ></i><i class='fa fa-share-alt'  aria-hidden='true'></i><i class='fa fa-paperclip'  aria-hidden='true'></i></div></div > </div>"
    )
  );
}

// Updated the pagination according to data fetched
function updatePagination() {
  console.log(currentData.length);
  $("#pagination").empty();
  $("#pagination").append(
    "<li class='page-item disabled'><a class='page-link' onClick='prevPageHandler()'  >Previous</a></li>"
  );
  for (var pageNo = startPage; pageNo < totalItems / 50 + 1; pageNo++) {
    $("#pagination").append(
      "<li class='page-item' id='page-" +
        pageNo +
        "'><a class='page-link' onClick='loadPageData(" +
        pageNo +
        ")' >" +
        pageNo +
        "</a></li>"
    );
  }
  $("#pagination").append(
    "<li class='page-item disabled'><a class='page-link' onClick='nextPageHandler()' >Next</a></li>"
  );
  //set current page active
  $("#page-" + currentPage).addClass("active");
}

// Handles click event on an image, stores scene_id in local store and redirects to scene page
function imageClickHandler(idx) {
  localStorage.setItem("selected_scene_id", currentData[idx].scene_id);
  window.location.href = "./scene.html";
}

// Next page Handler on Pagination
function nextPageHandler() {
  // if (parseInt(startpage) < '46') {
  startPage += 5;
  loadPageData(startPage);
  // }
}

// Prev page Handler on Pagination
function prevPageHandler() {
  // if (startpage > 5) {
  startPage -= 5;
  loadPageData(startPage);
  // }
}
