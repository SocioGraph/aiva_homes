let startPage = 1;
let activePage = 1;

$(document).ready(function() {
  if (!getCookie("authentication")) {
    signup();
  } else {
    const scene_id = localStorage.getItem("selected_scene_id");
    console.log(scene_id);
    get_scene(scene_id, {}, res => {
      updateSceneImage(res.data[0].image);
      // updatePagination();
      // currentData = res.data;
      console.log("scene data ", res.data), err => console.log(err);
    });
  }
});

function updateSceneImage(url) {
  console.log("url", url);
  $("#scene-image").append("<img src='" + url + "' alt='loading'/>");
}

function updateContainerData(data) {
  data.map((row, idx) =>
    $("#container").append(
      "<div class='card number-" +
        (idx % 3) +
        "'><div class='card-img-wrapper'><img src='" +
        row.image +
        "' alt='loading'/></div><div class='card-info-wrapper row'><div class='image-info col-md-7 col-md-offset-1'><p>Modern Home Workspace <br> PIDILITE</p></div><div class='options col-md-3'><i class='fa fa-heart' ></i><i class='fa fa-share-alt'  aria-hidden='true'></i><i class='fa fa-paperclip'  aria-hidden='true'></i></div></div > </div>"
    )
  );
}
