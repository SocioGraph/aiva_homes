# AIVA HOMES #

This repository is a front-end repository for Aiva Homes. In this project architects and designers can upload home interior inspirations


### How do I get set up? ###
* assets/js/*    Contains the the javascript files
* assets/css/*    Contains the css files
* assets/img/*    Contains the images
* assets/video/*   Contains the video files
* \*.html          Are the HTML templates

### Contribution guidelines ###
* Updating the API SDK for Dave

### Who do I talk to? ###
* ananth@iamdave.ai    9980838165
